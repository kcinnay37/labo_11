#pragma once

typedef struct card
{
	char nombre;
	char couleur;
	char* name[10];
	char valeuras;
}cards;

typedef struct somme
{
	char somme1;
	char somme2;
}somme;

typedef struct argent
{
	int bank;
	int mise;
	int profit;
}argent;

/*fonction pour piger les carte*/
void pigecard(cards* joueur, cards* croupier, char* tour);

/*fontction pour d�finir la somme des joueur*/
void setsomme(cards* joueur, cards* croupier, somme* sommes, char* tour);

/*fontion pour afficher les carte*/
void affichercards(cards* joueur, cards* croupier, somme* sommes, char* tour, argent* argents);

/*fonction pour definir le nom des carte*/
void setname(cards* joueur, cards* croupier);

/*fonction pour voir si il a eu un as en jeu et d�finir la valeur qu'il prend*/
void as(cards* joueur, cards* croupier, somme* sommes, char* tour);

/*fonction pour voir si le croupier pige*/
int pigecroupier(cards* croupier, somme* sommes);

/*fonction pour voir si un joueur obtient plus haut que 21*/
void depassemax(somme* sommes, argent* argents, int* arret);

/*fonction pour voir si un joueur obtient 21*/
void blackjack(somme* sommes, argent* argents, int* arret);

/*fonction pour verifier que il est pas de carte pareil*/
void pluspres(somme* sommes, argent* argents, int* arret);